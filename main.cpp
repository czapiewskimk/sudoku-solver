#include <iostream>
#include <string>
using namespace std;

void f_h_line(){
    cout << '+' << string(9, '-') << '+';
    cout << string(9, '-') << '+';
    cout << string(9, '-') << '+';
}

void f_print_sudoku(int sudoku[9][9]){
    f_h_line();
    for (int i{0}; i < 9; i++){
        cout << "\n" << '|';
        for (int j{0}; j < 9; j++){
            cout << " " << sudoku[i][j] << " ";
            if (!((j+1)%3)){
                cout << '|';
            }
        }
        if (!((i+1)%3)){
            cout << "\n";
            f_h_line();
        }
    }
}

void f_read_input(){
    int sudoku [9][9]{0};
    f_print_sudoku(sudoku);
}

int main() {
    f_read_input();
    return 0;
}

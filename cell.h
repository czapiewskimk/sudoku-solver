#ifndef SUDOKU_SOLVER_CELL_H
#define SUDOKU_SOLVER_CELL_H

#include <vector>

using namespace std;

class cell {
private:
    int value{0};
    int position[2];
    vector<int> possible{1, 2, 3, 4, 5, 6, 7, 8, 9};
public:
    void subscribe();
    bool only_possible();
    void erase_possible();
};


#endif //SUDOKU_SOLVER_CELL_H
